# frozen_string_literal: true

roles = Staff.roles.keys
availabilities = LogicalDelete::AVAILABILITIES.keys
flexible_years_range = (20..50).to_a
flexible_days_range = (1..30).to_a

puts '→ START 1: Generate staffs'
data = 300.times.map do
  availability = availabilities.sample
  deleted_at = availability == :unavailable ? (Date.current - flexible_days_range.sample.days) : nil
  {
    email: Faker::Internet.email,
    password: 'password',
    first_name: Faker::Name.name,
    last_name: Faker::Name.name,
    role: roles.sample,
    birthdate: Date.current - flexible_years_range.sample.years,
    phone_number: Faker::PhoneNumber.cell_phone_in_e164,
    address: Faker::Address.full_address,
    availability: availability,
    deleted_at: deleted_at
  }
end
Staff.create data
puts '✓ DONE 1: Generate staffs'
