# frozen_string_literal: true

flexible_years_range = (20..50).to_a

puts '→ START 1: Generate staffs'
Staff.create({
               email: 'super_administrator@default.data',
               password: 'password',
               first_name: 'Administrator',
               last_name: 'Super',
               role: :super_administrator,
               birthdate: Date.current - flexible_years_range.sample.years,
               phone_number: Faker::PhoneNumber.cell_phone_in_e164,
               address: Faker::Address.full_address,
               availability: :available
             })
puts '✓ DONE 1: Generate staffs'
