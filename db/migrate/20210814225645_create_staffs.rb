# frozen_string_literal: true

class CreateStaffs < ActiveRecord::Migration[6.1]
  def change
    create_table :staffs, unsigned: true do |t|
      # Core informations
      t.string   :email,          null: false
      t.string   :password_digest
      t.string   :first_name
      t.string   :last_name
      t.integer  :role,           limit: 1,   default: 0, unsigned: true

      # Addition informations
      t.date     :birthdate
      t.string   :phone_number
      t.string   :address

      # Reset password tokens
      t.string   :reset_password_token
      t.datetime :reset_password_token_sent_at

      # Logical delete
      t.integer  :availability,   limit: 1, default: 1, unsigned: true
      t.datetime :deleted_at

      t.timestamps null: false
    end

    add_index :staffs, :email, unique: true
  end
end
