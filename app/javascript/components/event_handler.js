const GAP_SYMBOL = '.'
const EVENT_NAMESPACE_POSITION = 0
const EVENT_TYPE_POSITION = 1
const WHITE_LIST = [
  // Plugins
  'plugins_datepickers',
  // Hooks
  'hooks_inputs', 'hooks_clicks'
]

/*
 * This class is used for avoiding event listeners conflicting between javascript files.
 * The name of event should be <namespace>.<event_type>
 * For example: 'document_templates.change' or 'document_templates.click'
 */

class EventHandlerClass {
  constructor () {
    // Function map data format:
    // { <namespace>: { func: <Function block>, oapts: <Options> } }
    this.functionsMap = {}
    this.currentNamespace = ''
  }

  _namespaceOf (eventString) {
    return eventString.split(GAP_SYMBOL)[EVENT_NAMESPACE_POSITION]
  }

  _typeOf (eventString) {
    return eventString.split(GAP_SYMBOL)[EVENT_TYPE_POSITION]
  }

  addEventListener (event, func, opts = {}) {
    // Check if it exists the event, remove it
    if (this.functionsMap[event]) {
      this.removeEventListener(event, opts)
    }

    // Add the new one
    this.functionsMap[event] = { func, opts }
    document.addEventListener(this._typeOf(event), this.functionsMap[event].func, opts)
  }

  removeEventListener (event, opts = {}) {
    document.removeEventListener(this._typeOf(event), this.functionsMap[event].func, opts)
  }

  switchEventListenersToNamespace (namespaceString) {
    if (this.currentNamespace === namespaceString) return

    this.currentNamespace = namespaceString
    const namespace = this._namespaceOf(namespaceString)
    const whiteLists = [ namespace, ...WHITE_LIST ]

    // Remove all events which are not owned by current namespace
    const notOwnedNames = Object.keys(this.functionsMap).filter((key) => {
      return whiteLists.every((whiteKey) => !key?.includes(whiteKey))
    })
    notOwnedNames.forEach((name) => this.removeEventListener(name, this.functionsMap[name].opts))

    // Add all events which are owned by current namespace
    const ownedNames = Object.keys(this.functionsMap).filter((key) => {
      return whiteLists.some((whiteKey) => key?.includes(whiteKey))
    })
    ownedNames.forEach((name) => this.addEventListener(name, this.functionsMap[name].func, this.functionsMap[name].opts))
  }
}

export const EventHandler = new EventHandlerClass()
