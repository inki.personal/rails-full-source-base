const NOTIFICATION_WRAPPER_ID = 'js-flash_notifications'

const Notification = {
  notifyScript: (scriptContent) => {
    let flashBlock = document.getElementById(NOTIFICATION_WRAPPER_ID)

    if (!flashBlock) {
      flashBlock = document.createElement('div')
      flashBlock.id = NOTIFICATION_WRAPPER_ID
      document.body.appendChild(flashBlock)
    }

    flashBlock.innerHTML = ''
    const range = document.createRange()
    range.setStart(flashBlock, 0)
    flashBlock.appendChild(range.createContextualFragment(scriptContent))
  }
}

export default Notification
