import 'assets/stylesheets/components/_loader.scss'

const renderLoaderNode = () => {
  const loaderObject = document.createElement('div')
  loaderObject.setAttribute('class', 'js-loader loader')
  loaderObject.innerHTML = `
    <div class='loader__backdrop'></div>
    <div class='loader__spinner'></div>
  `
  return loaderObject
}

const Loader = {
  enable: (selector = document.querySelector('body')) => {
    // Disable all existance loader
    Loader.disable(selector)

    // Create new loader and append it into selector as its child
    const loaderNode = renderLoaderNode()
    if (!selector.matches('body')) {
      loaderNode.classList.add('position-absolute')
    }
    else {
      loaderNode.classList.remove('position-absolute')
    }
    selector.appendChild(loaderNode)
    selector.dataset.isLoading = true
    selector.classList.add('is-loading')
  },
  disable: (selector = document.querySelector('body')) => {
    selector.dataset.isLoading = false
    selector.classList.remove('is-loading')
    const loaders = selector.querySelectorAll('.js-loader')
    if (loaders) Array.from(loaders).forEach((loader) => loader.remove())
  },
  disableAll: () => {
    const selectors = document.querySelectorAll('.js-loader')
    for (let i = 0; i < selectors.length; i++) {
      selectors[i].parentNode.dataset.isLoading = false
      selectors[i].parentNode.classList.remove('is-loading')
      selectors[i].remove()
    }
  },
  toggle: (selector = document.querySelector('body')) => {
    if (selector.dataset.isLoading) {
      Loader.disable(selector)
    }
    else {
      Loader.enable(selector)
    }
  },
  onLoad: (callback, selector = document.querySelector('body')) => {
    try {
      Loader.enable(selector)
      callback()
    }
    finally {
      Loader.disable(selector)
    }
  }
}

export default Loader
