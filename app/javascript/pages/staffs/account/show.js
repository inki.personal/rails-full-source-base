import { _f, _fAll } from '@helpers/selectors_helper'
import { EventHandler } from '@components/event_handler'
import { requireLocale } from '@helpers/locales_helper'

//* ************** CONSTANTS ***************//
const NAMESPACE = 'account_show'

const C_HIDDEN = 'd-none'
const S_BTN_EDIT = '.js-imageCropper-btnEdit'
const S_BTN_CANCEL = '.js-imageCropper-btnCancel'
const S_AVATAR_INFO = '.js-avatar-info'
const S_AVATAR_ANNOUNCEMENTS = '.js-avatar-announcements'

//* ************ VIEW HANDLERS *************//
EventHandler.addEventListener(`${NAMESPACE}.turbolinks:load`, () => {
  if (document.body.dataset.namespace !== NAMESPACE) return

  // Include croping avatar scripts
  require('@pages/shared/image_cropper')(NAMESPACE, {
    _f,
    _fAll,
    EventHandler,
    alert: window.Swal,
    messages: requireLocale(window.languages, 'plugins/croppr.js'),
    Croppr: require('croppr')
  })

  // [E] [Click] Edit avatar
  _f(S_BTN_EDIT).addEventListener('click', () => {
    // Show info
    _f(S_AVATAR_INFO).classList.remove(C_HIDDEN)
  })

  // [E] [Click] Cancel edit avatar
  _f(S_BTN_CANCEL).addEventListener('click', () => {
    // Remove announcements
    _f(S_AVATAR_ANNOUNCEMENTS).innerHTML = ''

    // Hide info
    _f(S_AVATAR_INFO).classList.add(C_HIDDEN)
  })
})
