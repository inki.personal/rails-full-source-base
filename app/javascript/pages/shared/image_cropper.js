module.exports = (namespace, helpers) => {
  require('croppr/src/css/croppr.css')

  //* ************** CONSTANTS ***************//
  // ====== Selectors ======== //
  const C_HIDDEN = 'd-none'

  const S_WRAPPER = '.js-imageCropper-wrapper'
  const S_IMAGE = '.js-imageCropper-image'
  const S_IMAGE_WRAPPER = '.js-imageCropper-imageWrapper'
  const S_IMAGE_INPUT = '.js-imageCropper-imageInput'
  const S_IMAGE_OFFSET_X = '.js-imageCropper-offsetX'
  const S_IMAGE_OFFSET_Y = '.js-imageCropper-offsetY'
  const S_IMAGE_WIDTH = '.js-imageCropper-width'
  const S_IMAGE_HEIGHT = '.js-imageCropper-height'
  const S_BTN_CHOOSE_FILE = '.js-imageCropper-btnChoose'
  const S_BTN_EDIT = '.js-imageCropper-btnEdit'
  const S_BTN_CANCEL = '.js-imageCropper-btnCancel'
  const S_BTN_SAVE = '.js-imageCropper-btnSave'
  const S_EDIT_GROUP_WRAPPER = '.js-imageCropper-editGroup'

  // ====== Constants ======== //
  const DATA_SEPARATOR_SYMBOL = '|'

  //* *************** ACTIONS ****************//
  const Cropper = {
    getOptions: (element) => {
      const options = { minSize: [ 50, 50, 'px' ] }

      const dataset = element.dataset
      if (dataset.aspectRatio) options.aspectRatio = +dataset.aspectRatio
      if (dataset.minSize) options.minSize = dataset.minSize.split(DATA_SEPARATOR_SYMBOL)
      if (dataset.maxSize) options.maxSize = dataset.maxSize.split(DATA_SEPARATOR_SYMBOL)
      if (dataset.startSize) options.startSize = dataset.startSize.split(DATA_SEPARATOR_SYMBOL)

      return options
    }
  }

  // Remove all edit buttons because of unsupport FileReader
  if (!FileReader) {
    const allBtnEdit = helpers._fAll(S_BTN_EDIT)
    if (allBtnEdit) allBtnEdit.forEach((btn) => btn.remove())
  }

  //* ************ VIEW HANDLERS *************//
  helpers.EventHandler.addEventListener(`${namespace}_imageCropper.click`, (e) => {
    // [E] [Click] Image Cropper: Edit
    if (e.target.matches(S_BTN_EDIT)) {
      const wrapper = e.target.closest(S_WRAPPER)
      const imageWrapper = helpers._f(S_IMAGE_WRAPPER, wrapper)

      // Hide edit button
      e.target.classList.add(C_HIDDEN)

      // Get the previous / base image
      const baseImage = imageWrapper.baseImage || helpers._f(S_IMAGE, imageWrapper)
      if (!imageWrapper.baseImage) imageWrapper.baseImage = baseImage.cloneNode(true)

      // Show edit buttons group
      const editGroupWrapper = helpers._f(S_EDIT_GROUP_WRAPPER, wrapper)
      editGroupWrapper.classList.remove(C_HIDDEN)

      // on file input loaded
      const imageInput = helpers._f(S_IMAGE_INPUT, wrapper)
      imageInput.onchange = (evt) => {
        const files = evt.target.files
        if (!files || files.length === 0) return

        // Check file mimetype
        const availableMimetypes = evt.target.dataset.availableMimetypes
        if (availableMimetypes && (new RegExp(availableMimetypes)).test(files[0].type) === false) {
          // Remove data of image input
          imageInput.value = ''
          return helpers.alert.error({ content: helpers.messages.unsupported_mime_type })
        }

        // Remove old copper
        const image = baseImage.cloneNode(true)
        imageWrapper.innerHTML = ''
        imageWrapper.appendChild(image)

        // Read image file data
        const fileReader = new FileReader()
        fileReader.onload = () => {
          image.src = fileReader.result

          // Find input doms
          const inputOffsetX = helpers._f(S_IMAGE_OFFSET_X, wrapper)
          const inputOffsetY = helpers._f(S_IMAGE_OFFSET_Y, wrapper)
          const inputWidth = helpers._f(S_IMAGE_WIDTH, wrapper)
          const inputHeight = helpers._f(S_IMAGE_HEIGHT, wrapper)

          // Init cropper
          new helpers.Croppr(S_IMAGE, {
            ...Cropper.getOptions(image),
            onCropEnd: (data) => {
              inputOffsetX.value = data.x
              inputOffsetY.value = data.y
              inputWidth.value = data.width
              inputHeight.value = data.height
            }
          })

          // Change choose file button's text
          const btnChooseFile = helpers._f(S_BTN_CHOOSE_FILE, wrapper)
          btnChooseFile.innerHTML = btnChooseFile.dataset.labelChosen

          // Enable button save
          helpers._f(S_BTN_SAVE, wrapper).disabled = false
        }

        // Read image files
        fileReader.readAsDataURL(files[0])
      }
    }

    // [E] [Click] Image Cropper: Cancel editing
    if (e.target.matches(S_BTN_CANCEL)) {
      const wrapper = e.target.closest(S_WRAPPER)

      // Hide editing buttons group
      const editGroupWrapper = helpers._f(S_EDIT_GROUP_WRAPPER, wrapper)
      editGroupWrapper.classList.add(C_HIDDEN)

      // Show edit button
      helpers._f(S_BTN_EDIT).classList.remove(C_HIDDEN)

      // Destroy image cropper
      const imageWrapper = helpers._f(S_IMAGE_WRAPPER, wrapper)
      imageWrapper.innerHTML = ''
      imageWrapper.appendChild(imageWrapper.baseImage)

      // Reset the image url
      const imageInput = helpers._f(S_IMAGE_INPUT, wrapper)
      imageInput.value = ''

      // Change choose file button's text
      const btnChooseFile = helpers._f(S_BTN_CHOOSE_FILE, wrapper)
      btnChooseFile.innerHTML = btnChooseFile.dataset.labelChoose

      // Disable save button
      const btnSave = helpers._f(S_BTN_SAVE, wrapper)
      btnSave.disabled = true
    }
  })
}
