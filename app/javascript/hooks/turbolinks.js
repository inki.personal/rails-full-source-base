const S_FLASH_NOTIFICATIONS_BLOCK = '#js-flash_notifications'

document.addEventListener('turbolinks:before-cache', () => {
  // Disable body loader before and after rendered
  window.Loader.disable()

  // Remove flash notifications before turbolinks cache
  const notificationsBlock = document.querySelector(S_FLASH_NOTIFICATIONS_BLOCK)
  if (notificationsBlock) notificationsBlock.innerHTML = ''
})
