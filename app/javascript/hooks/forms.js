import { S_DATEPICKER_NODE, S_DATEPICKER_RANGE_NODE } from '@configs/selectors'
import { bubbleClick } from '@helpers/events_helper'

const S_EVENT_SUBMIT_AFTER_RESET = '.js-form-resetBtn--submittable'
const S_EVENT_SUBMIT_AFTER_CHANGE = '.js-form-submitAfterChange'
const S_EVENT_SUBMIT_ON_EVENT = '.js-form--submitOnEvents'
const S_BTN_SUBMIT = '.js-form-submitBtn'
const DEFAULT_CHECKBOX_VALUES = [ '', 'all' ]

const DatePicker = {
  clear: (pickers) => {
    if (pickers.length === 0) return

    pickers.forEach((picker) => {
      if (picker?.datepicker) picker.datepicker.dates.length = 0
      picker.value = ''
    })
  }
}

const FormHandler = {
  resetElement: (element) => {
    switch (element.type.toLowerCase()) {
      case 'text':
      case 'password':
      case 'textarea':
      case 'hidden':
        element.value = ''
        break
      case 'select-one':
      case 'select-multi':
        element.value = ''
        if (element?.choices) element.choices.setChoiceByValue('')
        break
      case 'radio':
      case 'checkbox':
        if (element.checked) element.checked = false
        if (DEFAULT_CHECKBOX_VALUES.includes(element.value)) element.checked = true
        break
      default:
        break
    }
  }
}

document.addEventListener('turbolinks:load', () => {
  // BEGIN: Submit after reset
  document.querySelectorAll(S_EVENT_SUBMIT_AFTER_RESET).forEach((element) => {
    element.addEventListener('click', (e) => {
      e.preventDefault()
      // Reset the form
      const form = e.currentTarget.closest(S_EVENT_SUBMIT_ON_EVENT)
      const fieldElements = Array.from(form.elements)

      for (let i = 0; i < fieldElements.length; i++) {
        FormHandler.resetElement(fieldElements[i])
      }

      // Remove chosen dates
      DatePicker.clear(form.querySelectorAll(S_DATEPICKER_NODE))
      DatePicker.clear(form.querySelectorAll(S_DATEPICKER_RANGE_NODE))

      // Click submit button because of remote form behavior
      bubbleClick(form.querySelector(S_BTN_SUBMIT))
    })
  })

  // BEGIN: Submit after change
  document.querySelectorAll(S_EVENT_SUBMIT_AFTER_CHANGE).forEach((element) => {
    element.addEventListener('change', (e) => {
      const form = e.currentTarget.closest(S_EVENT_SUBMIT_ON_EVENT)
      // Click submit button because of remote form behavior
      bubbleClick(form.querySelector(S_BTN_SUBMIT))
    })
  })
})
