import { EventHandler } from '@components/event_handler'

const S_INPUT_TEXT = 'input[type="text"]'

// Trim input value
EventHandler.addEventListener('hooks_inputs.change', (e) => {
  if (e.target.matches(S_INPUT_TEXT)) {
    e.target.value = e.target.value.trim()
  }
})
