import { EventHandler } from '@components/event_handler'
import { bubbleClick } from '@helpers/events_helper'

const SELECTOR = '.js-loading--on_click'
const REFLECT_CLICKING_SELECTOR = '.js-reflect_click-btn'

EventHandler.addEventListener('hooks_clicks.click', (e) => {
  // [E] [Click] Show Loader
  if (e.target.matches(SELECTOR)) {
    window.Loader.enable()
  }

  // [E] [Click] Click another dom
  if (e.target.matches(REFLECT_CLICKING_SELECTOR)) {
    const targetDomSelector = e.target.dataset.reflectTarget
    const target = document.querySelector(targetDomSelector)
    if (target) bubbleClick(target)
  }
})
