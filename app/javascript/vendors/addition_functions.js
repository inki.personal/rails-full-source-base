const TRUE_VALUES = [ 1, '1', true, 'true' ]

const isTrue = (value) => TRUE_VALUES.includes(value)

const numberObject = Number
numberObject.prototype.isTrue = function () { return isTrue(`${this}`) }

const stringObject = String
stringObject.prototype.isTrue = function () { return isTrue(`${this}`) }
stringObject.prototype.addQueryParams = function (additionStringifyParams) {
  if (!additionStringifyParams) return this

  const joiningSymbol = this.toString().includes('?') ? '&' : '?'
  return `${this}${joiningSymbol}${additionStringifyParams}`
}

const booleanObject = Boolean
booleanObject.prototype.isTrue = function () { return isTrue(`${this}`) }
