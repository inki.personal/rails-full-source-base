// Plugin: Datepicker
export const S_DATEPICKER_NODE = '.js-datepicker'
export const S_DATEPICKER_RANGE_GROUP_NODE = '.input-daterange'
export const S_DATEPICKER_RANGE_NODE = '.js-datepicker--range'
export const S_DATE_FORMAT = 'meta[name="date-format"]'
export const S_DATE_MINIMUM_SELECTABLE_UNIX = 'meta[name="date-minimum-selectable-unix"]'
export const C_DATEPICKER_INITIALIZED = 'datepicker--initialized'
