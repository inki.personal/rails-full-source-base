import { config, library, dom } from '@fortawesome/fontawesome-svg-core'

// Solid
import {
  // Navigations
  faTachometerAlt, faBook, faSitemap, faFile, faLayerGroup, faBoxes,
  faMoneyBillWave, faUsers, faArrowLeft, faChalkboardTeacher, faSurprise, faTools,
  // Navbar
  faUser, faSignOutAlt,
  // Components: control_sidebar
  faSearch,
  // Sort
  faSort, faSortUp, faSortDown,
  // Record
  faPlus, faCog, faTimes, faEye,
  // User
  faInfoCircle, faKey, faEnvelope, faBirthdayCake, faPhoneSquareAlt, faMapMarkerAlt, faClock, faStopwatch, faPortrait
} from '@fortawesome/free-solid-svg-icons'

// Regular
import {
  faBell
} from '@fortawesome/free-regular-svg-icons'


library.add(
  // Navigations
  faTachometerAlt, faBook, faSitemap, faFile, faLayerGroup, faBoxes,
  faMoneyBillWave, faBell, faUsers, faArrowLeft, faChalkboardTeacher, faSurprise, faTools,
  // Navbar
  faUser, faSignOutAlt,
  // Components: control_sidebar
  faSearch,
  // Sort
  faSort, faSortUp, faSortDown,
  // Record
  faPlus, faCog, faTimes, faEye,
  // User
  faInfoCircle, faKey, faEnvelope, faBirthdayCake, faPhoneSquareAlt, faMapMarkerAlt, faClock, faStopwatch, faPortrait
)

config.mutateApproach = 'sync'
dom.watch()
