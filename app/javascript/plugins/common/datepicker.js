import { Datepicker, DateRangePicker } from 'vanillajs-datepicker'
import { bubbleChange } from '@/helpers/events_helper'
import { MILISECONDS } from '@configs/constants'
import {
  S_DATE_FORMAT,
  S_DATEPICKER_NODE,
  S_DATEPICKER_RANGE_NODE,
  S_DATEPICKER_RANGE_GROUP_NODE,
  S_DATE_MINIMUM_SELECTABLE_UNIX,
  C_DATEPICKER_INITIALIZED
} from '@configs/selectors'
import { requireLocale } from '@helpers/locales_helper'
import { EventHandler } from '@components/event_handler'

const DATE_PICKER_OPTIONS = {
  buttonClass: 'btn',
  clearBtn: true,
  todayBtn: true,
  todayBtnMode: 1,
  todayHighlight: true,
  orientation: 'top auto'
}

const state = {
  loadedLanguage: false
}

const Picker = {
  loadLocales: () => {
    if (state.loadedLanguage) return
    Datepicker.locales[window.languages.current] = requireLocale(window.languages, 'plugins/datepicker')
  },
  isDatePicker: (target) => {
    if (target instanceof HTMLDocument) return false
    return target.matches(S_DATEPICKER_NODE) && !target.classList.contains(C_DATEPICKER_INITIALIZED)
  },
  isDatePickerRange: (target) => {
    if (target instanceof HTMLDocument) return false
    return target.matches(S_DATEPICKER_RANGE_NODE) && !target.classList.contains(C_DATEPICKER_INITIALIZED)
  },
  getOptions: (target, defaults) => {
    const minimumSelectableUnix = parseInt(defaults.minimum_selectable_unix_obj?.content || 0)
    const minDate = target.dataset.pickerMinDate
    const maxDate = target.dataset.pickerMaxDate
    const pickerLevel = target.dataset.pickerLevel || 0
    const orientation = target.dataset.pickerOrientation || 'auto'
    const clearBtn = target.dataset.pickerClearBtn ? target.dataset.pickerClearBtn.isTrue() : true

    return {
      language: window.languages.current,
      format: target.dataset.pickerFormat || document.querySelector(S_DATE_FORMAT).content,
      minDate: new Date((minDate || minimumSelectableUnix) * MILISECONDS),
      maxDate: (maxDate ? new Date(parseInt(maxDate) * MILISECONDS) : null),
      pickLevel: pickerLevel,
      orientation,
      clearBtn
    }
  }
}

document.addEventListener('turbolinks:load', () => {
  // BEGIN: Datepicker
  EventHandler.addEventListener('plugins_datepickers.focus', (e) => {
    if (Picker.isDatePicker(e.target)) {
      // Set datepicker language
      Picker.loadLocales()

      // Init datepicker
      new Datepicker(e.target, {
        ...DATE_PICKER_OPTIONS,
        ...{ autohide: true },
        ...Picker.getOptions(e.target, {
          minimum_selectable_unix_obj: document.querySelector(S_DATE_MINIMUM_SELECTABLE_UNIX)
        })
      })

      // Add initialized klass flag
      e.target.classList.add(C_DATEPICKER_INITIALIZED)

      // Add [event] listener
      e.target.addEventListener('changeDate', () => bubbleChange(e.target))
    }

    if (Picker.isDatePickerRange(e.target)) {
      const rangeBlock = e.target.closest(S_DATEPICKER_RANGE_GROUP_NODE)
      // Set datepicker language
      Picker.loadLocales()

      // Init datepicker
      new DateRangePicker(rangeBlock, {
        ...DATE_PICKER_OPTIONS,
        ...Picker.getOptions(e.target, {
          minimum_selectable_unix_obj: document.querySelector(S_DATE_MINIMUM_SELECTABLE_UNIX)
        })
      })

      // Add initialized klass flag
      e.target.classList.add(C_DATEPICKER_INITIALIZED)

      // Add [event] listener
      e.target.addEventListener('changeDate', () => bubbleChange(e.target))
    }
  }, true)
})
