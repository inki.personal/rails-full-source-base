import 'choices.js/public/assets/styles/choices.css'
import { requireLocale } from '@helpers/locales_helper'
import { ajax } from '@rails/ujs'
const Choices = require('choices.js')

//* ************** CONSTANTS ***************//
const S_SELECT = '.js-searchableSelect:not(.initialized)'
const S_SELECT_LAZY_LOAD_WRAPPER = '.js-searchableSelect--lazyLoadWrapper'
const C_INITIALIZED = 'initialized'
const C_SELECT_LAZY_LOAD = 'js--selectLazyLoad'

//* *************** ACTIONS ****************//
const LazyLoad = async (targets) => {
  // Enable loader into wrapper for avoiding user actions
  const choiceWrapper = targets.currentTarget.closest(S_SELECT_LAZY_LOAD_WRAPPER)
  window.Loader.enable(choiceWrapper)

  ajax({
    url: targets.currentTarget.dataset.url,
    type: 'GET',
    async: true,
    complete: (_) => {
      // Disable wrapper loading
      window.Loader.disable(choiceWrapper)
    },
    success: (response) => {
      if (response.status) {
        targets.context.setChoices([ ...targets.context.config.choices, ...response.records ], 'value', 'label', true)
        // Set selected value
        const selectedValue = targets.currentTarget.dataset.selectedValue
        if (selectedValue) targets.context.setChoiceByValue(+selectedValue)
      }
    }
  })
}

const SearchableSelect = {
  init: () => {
    const messages = requireLocale(window.languages, 'plugins/choice.js')
    const elements = document.querySelectorAll(S_SELECT)

    for (let i = 0; i < elements.length; i++) {
      if (elements[i].choices) return

      elements[i].choices = new Choices(elements[i], {
        // Locales
        searchPlaceholderValue: messages.searchPlaceholderValue,
        loadingText: messages.loadingText,
        noResultsText: messages.noResultsText,
        noChoicesText: messages.noChoicesText,
        itemSelectText: messages.itemSelectText,
        position: elements[i].dataset.searchableSelectPosition || 'bottom',
        callbackOnInit () {
          elements[i].classList.add(C_INITIALIZED)
          // BEGIN: Lazy loading choices
          if (elements[i].classList.contains(C_SELECT_LAZY_LOAD)) {
            LazyLoad({ context: this, currentTarget: elements[i] })
          }
        },
        callbackOnCreateTemplates (template) {
          return {
            choice: (classNames, data) => {
              const disabledClass = data.disabled ? classNames.itemDisabled : classNames.itemSelectable
              return template(`
                <div class="${classNames.item} ${classNames.itemChoice} ${disabledClass}"
                     data-custom-properties="${data.customProperties}"
                     data-select-text="${this.config.itemSelectText}"
                     data-choice ${data.disabled ? 'data-choice-disabled aria-disabled="true"' : 'data-choice-selectable'}
                     data-id="${data.id}"
                     data-value="${data.value}"
                     ${data.groupId > 0 ? 'role="treeitem"' : 'role="option"'}>
                  ${data.label}
                </div>
              `)
            }
          }
        }
      })

      // Add handle disabled/enabled event change select
      elements[i].addEventListener('change', () => {
        if (elements[i].disabled) {
          elements[i].choices.disable()
        }
        else {
          elements[i].choices.enable()
        }
      })
    }
  }
}

// export default SearchableSelect
window.SearchableSelect = SearchableSelect
window.Choices = Choices
