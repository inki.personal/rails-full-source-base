import 'sweetalert2/dist/sweetalert2.min.css'
import SweetAlert from 'sweetalert2/dist/sweetalert2.min'
import { requireLocale } from '@helpers/locales_helper'

const DEFAULT_SETTINGS = {
  position: 'center',
  showConfirmButton: false,
  timer: 5000
}

const Swal = {
  _language: null,
  _messages: null,
  loadMessages: (languages) => {
    Swal._language = languages
    Swal._messages = requireLocale(window.languages, 'plugins/sweet_alert_confirm')
  },
  error: (data) => {
    return new SweetAlert({
      icon: 'error',
      title: data.title || Swal._messages.titles.error,
      html: data.content,
      ...DEFAULT_SETTINGS
    })
  },
  warning: (data) => {
    return new SweetAlert({
      icon: 'warning',
      title: data.title || Swal._messages.titles.warning,
      html: data.content,
      ...DEFAULT_SETTINGS
    })
  },
  info: (data) => {
    return new SweetAlert({
      icon: 'info',
      title: data.title || Swal._messages.titles.info,
      html: data.content,
      ...DEFAULT_SETTINGS
    })
  },
  success: (data) => {
    return new SweetAlert({
      icon: 'success',
      title: data.title || Swal._messages.titles.success,
      html: data.content,
      ...DEFAULT_SETTINGS
    })
  },
  confirm: (data) => {
    return new SweetAlert({
      title: data.title || Swal._messages.confirm.title,
      html: data.content,
      icon: data.icon || 'warning',
      showCancelButton: true,
      confirmButtonText: data.confirmBtnText || Swal._messages.confirm.confirmBtn,
      cancelButtonText: data.cancelBtnText || Swal._messages.confirm.cancelBtn
    })
  }
}

document.addEventListener('turbolinks:load', () => {
  Swal.loadMessages(window.languages)
})
window.Swal = Swal
