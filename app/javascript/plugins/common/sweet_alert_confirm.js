import Rails from '@rails/ujs'
import { bubbleChange, bubbleClick, bubbleSubmit } from '@helpers/events_helper'

// BEGIN: Variables Declaration
const confirmInfo = {
  status: false
}

const ELEMENT_EVENT_MAPPINGS = {
  click: `a[data-confirm], a[data-method], a[data-remote]:not([disabled]), a[data-disable-with],
          a[data-disable], button[data-remote]:not([form]), button[data-confirm]:not([form]),
          form input[type=submit], form input[type=image], form button[type=submit],
          form button:not([type]), input[type=submit][form], input[type=image][form],
          button[type=submit][form], button[form]:not([type])`,
  change: 'select[data-remote], input[data-remote], textarea[data-remote]',
  submit: 'form'
}
// END: Variables Declaration

// BEGIN: Methods
const __trigger = (element) => {
  const eventType = Object.keys(ELEMENT_EVENT_MAPPINGS).find((key) => element.matches(ELEMENT_EVENT_MAPPINGS[key]))
  switch (eventType) {
    case 'click':
      bubbleClick(element)
      break
    case 'change':
      bubbleChange(element)
      break
    case 'submit':
      bubbleSubmit(element)
      break
    default:
      break
  }
}
// END: Methods

// BEGIN: Override rails/ujs confirmation dialog
Rails.confirm = (message, el) => {
  const data = el.dataset

  if (confirmInfo.status) {
    confirmInfo.status = false
    return true
  }

  window.Swal.confirm({
    title: data.confirmTitle,
    content: message,
    icon: data.confirmIcon,
    languages: window.languages
  }).then((response) => {
    if (response.value) {
      confirmInfo.status = true
      window.Loader.enable()
      __trigger(el)
    }
  })

  return false
}
// END: Override Railsujs confirmation dialog
