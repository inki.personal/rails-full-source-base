import { config, library, dom } from '@fortawesome/fontawesome-svg-core'

// Solid
import {
  faUserSecret, faUnlock
} from '@fortawesome/free-solid-svg-icons'

library.add(
  faUserSecret, faUnlock
)

config.mutateApproach = 'sync'
dom.watch()
