// import { Tooltip, Popover } from 'bootstrap'

// // Popovers
// // Note: Disable this if you're not using Bootstrap's Popovers
// const popoverTriggerList = [].slice.call(document.querySelectorAll('[data-toggle="popover"]'))
// popoverTriggerList.map((popoverTriggerEl) => {
//   return new Popover(popoverTriggerEl)
// })

// // Tooltips
// // Note: Disable this if you're not using Bootstrap's Tooltips
// const tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-toggle="tooltip"]'))
// tooltipTriggerList.map((tooltipTriggerEl) => {
//   return new Tooltip(tooltipTriggerEl)
// })

// Modals
document.addEventListener('turbolinks:load', () => {
  document.body.addEventListener('shown.bs.modal', (e) => {
    e.target.dataset.modalInitialized = true
    const bootstrapBackdrop = document.querySelectorAll('.modal-backdrop')
    if (bootstrapBackdrop.length < 2) return

    for (let i = 1; i < bootstrapBackdrop.length; i++) bootstrapBackdrop[i].remove()
  })

  document.body.addEventListener('hidden.bs.modal', (e) => {
    e.target.dataset.modalInitialized = true
    const bootstrapBackdrop = document.querySelectorAll('.modal-backdrop')
    if (bootstrapBackdrop.length === 0) return

    document.classList.remove('modal-open')
    for (let i = 1; i < bootstrapBackdrop.length; i++) bootstrapBackdrop[i].remove()
  })
})
