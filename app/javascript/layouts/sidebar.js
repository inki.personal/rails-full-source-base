const S_WRAPPER = '.js-simplebar'
const S_BTN_TOGGLE = '.sidebar-toggle'
const C_FLG_COLLAPSE = 'collapsed'

document.addEventListener('turbolinks:load', () => {
  const simpleBarElement = document.querySelector(S_WRAPPER)

  if (simpleBarElement) {
    const sidebarToggleElement = document.querySelector(S_BTN_TOGGLE)

    sidebarToggleElement.addEventListener('click', () => {
      simpleBarElement.classList.toggle(C_FLG_COLLAPSE)

      simpleBarElement.addEventListener('transitionend', () => {
        window.dispatchEvent(new Event('resize'))
      })
    })
  }
})
