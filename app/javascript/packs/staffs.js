import Loader from '@components/loader'
import Bootstrap from 'bootstrap/dist/js/bootstrap.min'
import Notification from '@components/notification'
import { EventHandler } from '@components/event_handler'
import 'bootstrap/dist/css/bootstrap.min'
import '@stylesheets/staffs.scss'

/******************
 * CORE
 ******************/
require('@rails/ujs').start()
require('turbolinks').start()

/******************
 * LOAD ASSETS
 ******************/
require.context('@images', true)

/******************
 * LOAD VENDORS
 ******************/
require('@vendors/addition_functions')

/******************
 * THEMES
 ******************/
window.bootstrap = Bootstrap
require('@layouts/bootstrap')
require('@layouts/sidebar')

/******************
 * PLUGINS
 ******************/
window.Loader = Loader
window.Notification = Notification
require('@plugins/staffs/fontawesome')
require('@plugins/common/sweet_alert')
require('@plugins/common/sweet_alert_confirm')
require('@plugins/common/datepicker')
require('@plugins/common/searchable_select')

/******************
 * HOOKERS
 ******************/
require('@hooks/forms')
require('@hooks/inputs')
require('@hooks/clicks')
require('@hooks/turbolinks')

/******************
 * HOOKERS
 ******************/
require('@pages/staffs/account/show')

/******************
 * PAGES
 ******************/
document.addEventListener('turbolinks:render', () => {
  EventHandler.switchEventListenersToNamespace(document.body.dataset.namespace)
})
