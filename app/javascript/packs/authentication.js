import Loader from '@components/loader'
import Bootstrap from 'bootstrap/dist/js/bootstrap.min'
import Notification from '@components/notification'
import { EventHandler } from '@components/event_handler'
import '@stylesheets/authentication.scss'

/******************
 * CORE
 ******************/
require('@rails/ujs').start()
require('turbolinks').start()

window.Loader = Loader

/******************
 * LOAD ASSETS
 ******************/
require.context('@images', true)

/******************
 * THEMES
 ******************/
window.bootstrap = Bootstrap
require('@layouts/bootstrap')

/******************
 * HOOKERS
 ******************/
require('@hooks/turbolinks')

/******************
 * PLUGINS
 ******************/
window.Loader = Loader
window.Notification = Notification
require('@plugins/authentication/fontawesome')
require('@plugins/common/sweet_alert')

document.addEventListener('turbolinks:render', () => {
  EventHandler.switchEventListenersToNamespace(document.body.dataset.namespace)
})
