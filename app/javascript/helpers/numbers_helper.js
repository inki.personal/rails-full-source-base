export const parseIntRescue = (numeric) => {
  try {
    return parseInt(numeric)
  }
  catch (err) {
    return 0
  }
}
