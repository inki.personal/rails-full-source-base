// Find
export const _f = (selector, parent = document) => parent.querySelector(selector)
// Find All
export const _fAll = (selector, parent = document) => parent.querySelectorAll(selector)
export const findById = (selector, parent = document) => parent.getElementById(selector)
export const findByClass = (selector, parent = document) => parent.getElementsByClassName(selector)
export const findByTag = (selector, parent = document) => parent.getElementsByTagName(selector)
