export const isPathNameOfString = (pathname, urlString) => {
  return (new URL(urlString).pathname.replace(/\/$/, '') === pathname) || urlString?.match(pathname)
}
