export const requireLocale = (languages, path) => {
  try {
    return require(`@locales/${languages.current}/${path}.json`)
  }
  catch (e) {
    return require(`@locales/${languages.default}/${path}.json`)
  }
}
