const INDEX_NAME_REGEX = /\[(\d+)\]/
const INDEX_ID_REGEX = /_(\d+)_/

export const normalize = (text) => {
  return (text || '').normalize('NFD').replace(/[\u0300-\u1e949]/g, '').replace('đ', 'd')
}

export const toArray = (text, separate_symbol = ',') => {
  return (text || '').split(separate_symbol).filter(Boolean)
}

export const ramdomString = () => {
  return Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15)
}

export const reIndexName = (name, replacementValue, atPosition = 'last') => {
  return __replaceStringData({
    string: name,
    splitRegex: INDEX_NAME_REGEX,
    replacementValue,
    atPosition,
    openBracket: '[',
    closeBracket: ']'
  })
}

export const reIndexId = (id, replacementValue, atPosition = 'last') => {
  return __replaceStringData({
    string: id,
    splitRegex: INDEX_ID_REGEX,
    replacementValue,
    atPosition,
    openBracket: '_',
    closeBracket: '_'
  })
}

export const format = (content, replacementsObject) => {
  let resultContent = `${content}`
  for (const [ key, value ] of Object.entries(replacementsObject)) {
    resultContent = resultContent.replace(key, value)
  }
  return resultContent
}

// Private

const __replacementPosition = (length, value) => {
  // -1 for counting from 0
  // -1 for last character is breaking symbol
  if (value === 'last') return length - 2
  if (value === 'first') return 1
  return value
}

const __replaceStringData = (data) => {
  const splitedData = `${data.string}`.split(data.splitRegex)
  if (splitedData.length === 0) return data.string

  const replacementAtPosition = __replacementPosition(splitedData.length, data.atPosition)
  return splitedData.reduce((result, currentValue, currentIndex) => {
    let lastValue = currentValue
    if (replacementAtPosition === currentIndex) lastValue = data.replacementValue
    if (currentIndex % 2 === 0) return `${result}${lastValue}`
    return `${result}${data.openBracket}${lastValue}${data.closeBracket}`
  })
}
