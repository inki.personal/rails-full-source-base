export const bubbleClick = (_el) => {
  _el.dispatchEvent(
    new MouseEvent('click', {
      bubbles: true,
      cancelable: true,
      view: window
    })
  )
}

export const bubbleChange = (_el) => {
  _el.dispatchEvent(
    new Event('change', {
      bubbles: true,
      cancelable: true
    })
  )
}

export const bubbleSubmit = (_el) => {
  _el.dispatchEvent(
    new Event('submit', {
      bubbles: true,
      cancelable: true
    })
  )
}
