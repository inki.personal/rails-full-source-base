# frozen_string_literal: true

class StorageFileValidator < ActiveModel::EachValidator
  AVAILABLE_OPTIONS = %i[size_maximum content_type].freeze
  FILE_SIZES = %w[B KB MB GB TB Pb EB ZB].freeze
  ONE_MEGABYTE = 1_000

  def validate_each(record, attribute, value)
    options.slice(*AVAILABLE_OPTIONS).each_key do |rule|
      send("validate_#{rule}", record, attribute, value, options[rule])
    end
  end

  private

  def validate_content_type(record, attribute, vaue, data)
    return if vaue.attachment.blank? || data.include?(vaue.attachment.blob.content_type)

    default_message = I18n.t('errors.messages.storage_file_invalid_content_type', types: data.join(', '))
    record.errors.add attribute, (options[:message] || default_message)
  end

  def validate_size_maximum(record, attribute, value, data)
    return if value.attachment.blank? || value.attachment.byte_size <= data

    default_message = I18n.t('errors.messages.storage_file_size_maximum', maximum_size: file_size_with_type(data))
    record.errors.add attribute, (options[:message] || default_message)
  end

  def file_size_with_type(size)
    return '0.0 B' if size == 0

    exp = (Math.log(size) / Math.log(ONE_MEGABYTE)).to_i
    exp += 1 if size.to_f / ONE_MEGABYTE**exp >= ONE_MEGABYTE - 0.05
    exp = FILE_SIZES.size - 1 if exp > FILE_SIZES.size - 1

    file_size_to_text(size, exp)
  end

  def file_size_to_text(size, exp)
    format('%<size>.2f %<size_type>s', size: size.to_f / ONE_MEGABYTE**exp, size_type: FILE_SIZES[exp])
  end
end
