# frozen_string_literal: true

class DateTimeRangeValidator < ActiveModel::EachValidator
  AVAILABLE_OPTIONS = %i[
    between greater_than_or_equal_to less_than_or_equal_to greater_than less_than
    greater_than_year less_than_or_eql_to_year
  ].freeze

  def validate_each(record, attribute, value)
    options.slice(*AVAILABLE_OPTIONS).each_key do |rule|
      send("validate_#{rule}", record, attribute, value, value_of(options[rule], record))
    end
  end

  private

  def date_with_time(date)
    I18n.l date, format: :date_with_time, default: nil
  end

  def value_of(data, source_record)
    return source_record[data] if data.is_a? Symbol
    return data unless data.is_a? Proc

    data.call
  end

  def validate_between(record, attribute, value, data)
    return if value && (data.first <= value) && (data.last >= value)

    default_message = I18n.t('errors.messages.not_in_date_range', from: date_with_time(data.first),
                                                                  to: date_with_time(data.last))
    record.errors.add attribute, (options[:message] || default_message)
  end

  def validate_greater_than_or_equal_to(record, attribute, value, data)
    return if value && value >= data

    default_message = I18n.t('errors.messages.greater_than_or_equal_to_date', date: date_with_time(data))
    record.errors.add attribute, (options[:message] || default_message)
  end

  def validate_less_than_or_equal_to(record, attribute, value, data)
    return if value && value <= data

    default_message = I18n.t('errors.messages.less_than_or_equal_to_date', date: date_with_time(data))
    record.errors.add attribute, (options[:message] || default_message)
  end

  def validate_greater_than(record, attribute, value, data)
    return if value && value > data

    default_message = I18n.t('errors.messages.greater_than_date', date: date_with_time(data))
    record.errors.add attribute, (options[:message] || default_message)
  end

  def validate_less_than(record, attribute, value, data)
    return if value && value < data

    default_message = I18n.t('errors.messages.less_than_date', date: date_with_time(data))
    record.errors.add attribute, (options[:message] || default_message)
  end

  def validate_less_than_or_eql_to_year(record, attribute, value, data)
    return if value && value.year <= data

    default_message = I18n.t('errors.messages.less_than_or_eql_to_year', year: data)
    record.errors.add attribute, (options[:message] || default_message)
  end

  def validate_greater_than_year(record, attribute, value, data)
    return if value && value.year > data

    default_message = I18n.t('errors.messages.greater_than_year', year: data)
    record.errors.add attribute, (options[:message] || default_message)
  end
end
