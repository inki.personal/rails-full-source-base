# frozen_string_literal: true

require 'mini_magick'

class Account::UpdatingAvatarService
  attr_reader :record

  def initialize(record, params)
    @record = record
    @params = params
  end

  def execute!
    validate_avatar!

    ActiveRecord::Base.transaction do
      crop_avatar!
      update_avatar!
    rescue StandardError => e
      CustomLogger.instance.unhandled_error(e)
      raise ActionError.new(:account, :can_not_upload_avatar)
    end
  end

  private

  def validate_avatar!
    record.avatar = @params[:avatar]
    raise ActiveRecord::RecordInvalid, record if record.invalid?
  end

  ### Cropping Data Format: <width> x <height> + <offset_x> + <offset_y>
  def cropping_data
    "#{@params[:width]}x#{@params[:height]}+#{@params[:offset_x]}+#{@params[:offset_y]}"
  end

  def crop_avatar!
    MiniMagick::Image.new(@params[:avatar].tempfile.path)
                     .crop(cropping_data)
                     .resize(Settings.image.avatar_size)
  rescue StandardError => e
    CustomLogger.instance.unhandled_error(e)
    raise ActionError.new(:image, :can_not_crop_image)
  end

  def update_avatar!
    record.avatar = @params[:avatar]
    record.save!
  end
end
