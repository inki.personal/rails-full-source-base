# frozen_string_literal: true

class Account::ChangingPasswordService
  include ActiveModel::Model
  include ActiveModel::Attributes

  attr_reader :record

  attribute :old_password, :string
  attribute :password, :string
  attribute :password_confirmation, :string

  validates_presence_of :old_password, :password, :password_confirmation
  validates :password, confirmation: true, if: -> { password.present? && password_confirmation.present? }
  validate :validate_old_password_must_be_exactly, if: -> { old_password.present? }

  def of_record(record)
    @record = record
    self
  end

  def change!
    raise ActiveRecord::RecordInvalid, self if invalid?

    @record.password = password
    @record.save validate: false
  end

  private

  def validate_old_password_must_be_exactly
    return if @record.authenticate(password)

    errors.add(:old_password, :wrong)
  end
end
