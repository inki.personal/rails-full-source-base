# frozen_string_literal: true

class Staffs::Auth::SigningInService
  attr_reader :staff

  def initialize(params)
    @params = params
  end

  def verify!
    @staff = Staff.find_by email: @params[:email]
    verify_email_and_password!
    verify_staff_account_status!
  end

  private

  def verify_email_and_password!
    return if @staff.present? && @staff.authenticate(@params[:password])

    raise ::ActionError.new(:auth, :email_or_password_invalid)
  end

  def verify_staff_account_status!
    return if @staff.available?

    raise ::ActionError.new(:account, :unavailable)
  end
end
