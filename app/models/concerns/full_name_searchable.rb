# frozen_string_literal: true

module FullNameSearchable
  extend ActiveSupport::Concern

  included do
    ransacker :full_name do |parent|
      name_positions = I18n.t('human.names_order', default: { first: :first_name, last: :last_name })
      Arel::Nodes::NamedFunction.new('CONCAT_WS', [
                                       Arel::Nodes.build_quoted(' '),
                                       parent.table[name_positions[:first]],
                                       parent.table[name_positions[:last]]
                                     ])
    end
  end
end
