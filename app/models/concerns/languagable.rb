# frozen_string_literal: true

module Languagable
  extend ActiveSupport::Concern

  included do
    enum language: Settings.language.available, _suffix: true

    validates :language, presence: true
    validates :language, inclusion: { in: languages.keys }, if: -> { language.present? }
  end
end
