# frozen_string_literal: true

class Staff < ApplicationRecord
  has_secure_password

  ###### BEGIN: Inclusion ########
  include LogicalDelete
  include FullNameSearchable

  ###### BEGIN: Enumerables ########
  enum role: { standard: 0, manager: 1, super_administrator: 9 }

  ###### Storables ########
  has_one_attached :avatar

  ###### BEGIN: Validations ########
  # Validation: Core informations
  validates_presence_of :email, :role, :first_name, :last_name
  validates_format_of :email, with: Settings.regex.email, if: -> { email.present? }
  validates :role, inclusion: { in: roles.keys }
  validates_length_of :email, :first_name, :last_name, :reset_password_token,
                      :phone_number, :address, maximum: 255
  validates :avatar, storage_file: {
    size_maximum: 300.kilobytes,
    content_type: ['image/png', 'image/jpg', 'image/jpeg']
  }
  # Validation: Addition informations
  validates_format_of :phone_number, with: Settings.regex.phone_number, if: -> { phone_number.present? }
  validates_length_of :address, maximum: 255
  validates :birthdate, date_time_range: {
    greater_than_or_equal_to: proc { |_| Date.current - Settings.datetime.maximum_birthdate_gap.years },
    less_than_or_equal_to: proc { |_| Date.current + Settings.datetime.minimum_birthdate_gap.years }
  }, if: -> { birthdate.present? }

  ###### BEGIN: Getters ########
  def names(before_changed: false)
    return { first: first_name, last: last_name } unless before_changed

    { first: first_name_was, last: last_name_was }
  end

  def avatar_image
    @avatar_image ||= if avatar.attached?
                        avatar
                      else
                        Settings.image.default_avatar
                      end
  end
end
