# frozen_string_literal: true

class ActionError < StandardError
  attr_accessor :message, :is_permission_error

  def initialize(*target_error_type, **options)
    if options[:object].present?
      fetch_data_from_object(options[:object])
    else
      self.is_permission_error = options[:permission_error].present?
      fetch_message_from_attributes(*target_error_type, default: options[:default])
    end
  end

  def permission_error?
    is_permission_error
  end

  def self.===(_)
    true
  end

  private

  def fetch_data_from_object(error_record)
    if error_record.instance_of?(::ActionError)
      self.is_permission_error = error_record.permission_error?
      self.message = error_record.message
    else
      fetch_message_from_attributes(:framework, error_record.class.name.underscore)
    end
  end

  def fetch_message_from_attributes(*target_error_type, default: [:undefined, :undefined])
    target, error_type = target_error_type
    self.message = I18n.t("action_error.#{target}.#{error_type}", default: '')
    return if message.present?

    fetch_message_from_attributes(*default)
  end
end
