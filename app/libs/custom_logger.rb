# frozen_string_literal: true

class CustomLogger
  def initialize
    # Setup log paths
    @unhandled = init_logger(Settings.logger.unhandled_error_path)
  end

  # BEGIN: Initialization
  # Init logger
  def init_logger(path)
    Logger.new path, Settings.logger.files_amount, Settings.logger.maximum_file_size.megabytes
  end
  # END: Initialization

  # BEGIN: Logging
  # Log unhandled error
  def unhandled_error(info)
    @unhandled.error error_content_of(info)
  end
  # END: Logging

  class << self
    attr_reader :instance
  end

  @instance = new

  private_class_method :new

  private

  # Build error content
  def error_content_of(error_info)
    <<~TEXT
      \n===== BEGIN: Error =====
      Message: #{error_info.try(:message)}
      Backtraces:
      - #{error_info.try(:backtrace)
                    .try(:take, Settings.logger.backtrace_lines)
                    .try(:join, "\n- ")}
      ===== END: Error =====
    TEXT
  end
end
