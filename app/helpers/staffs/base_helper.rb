# frozen_string_literal: true

module Staffs::BaseHelper
  include Pagy::Frontend

  include Common::HumansHelper
  include Common::TextsHelper
  include Common::TitlesHelper
  include Common::ParamsHelper
  include Common::RecordsHelper
  include Common::LanguagesHelper
  include Common::PaginationsHelper
  include Common::NotificationsHelper::Frontend

  # BEGIN: Layouts
  def breadcrumbs(**items)
    content_for :breadcrumbs, render('layouts/staffs/breadcrumb', items: items)
  end

  def navigation_item_name(content)
    return I18n.t('navigations.undefined') if content.blank?

    I18n.t "navigations.staff.#{content}", default: content.to_s
  end
end
