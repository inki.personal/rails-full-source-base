module ApplicationHelper
  # The namespace for turbolinks js
  def js_namespace
    "#{controller_name}_#{action_name}"
  end
end
