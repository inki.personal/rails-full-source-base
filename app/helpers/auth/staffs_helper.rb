# frozen_string_literal: true

module Auth::StaffsHelper
  extend ActiveSupport::Concern

  included do
    helper_method :warden, :current_staff, :staff_signed_in?, :authenticate_staff!
  end

  private

  def staff_signed_in?
    current_staff.present?
  end

  def current_staff
    warden.user(:staff)
  end

  def warden
    request.env['warden']
  end

  def authenticate_staff!
    # Redirect back to sign in page if staff did not sign in
    unless staff_signed_in?
      return redirect_to staffs_sign_in_path, flash: { error: I18n.t('notifications.auth.you_must_sign_in_before') }
    end

    # Return if staff signed in
    return if current_staff.available?

    # Sign out and Redirect to sign in page if staff is deactived
    warden.logout(:staff)
    redirect_to staffs_sign_in_path, flash: { error: I18n.t('notifications.auth.your_account_has_been_deactived') }
  end
end
