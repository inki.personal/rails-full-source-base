# frozen_string_literal: true

module Common::NotificationsHelper
  ERROR_KINDS = %w[notice success alert error info warning].freeze
  KIND_INFO = {
    success: { type: 'success', klass: 'alert-success', icon: 'fa-check' },
    error: { type: 'error', klass: 'alert-danger', icon: 'fa-ban' },
    info: { type: 'info', klass: 'alert-info', icon: 'fa-info' },
    warning: { type: 'warning', klass: 'alert-warning', icon: 'fa-exclamation-triangle' }
  }.freeze

  # For Controllers including
  module Backend
    include Common::NotificationsHelper

    def notify(kind, message, now: false)
      if now
        flash.now[kind] = message
      else
        flash[kind] = message
      end
    end

    def notify_common(model, action, kind, now: false)
      message = I18n.t("notifications.common.#{action}_#{kind}", name: model.model_name.human.downcase)
      notify kind, message, now: now
    end
  end

  # For Views including
  module Frontend
    include Common::NotificationsHelper

    def render_announcements(kind, *contents, alert_klass: '', removable: true)
      return if ERROR_KINDS.exclude? kind.to_s

      render 'shared/announcements/contents',
             info: announcement_kind_info(kind.to_s),
             contents: contents,
             alert_klass: alert_klass,
             removable: removable
    end

    def render_flash_notifications
      notification_types = flash.keys & ERROR_KINDS
      return if notification_types.empty?

      notifications = notification_types.to_h do |t|
        [announcement_kind_info(t)[:type], flash[t]]
      end
      render 'shared/notifications/flash_notifications', notifications: notifications
    end
  end

  private

  def announcement_kind_info(kind_name)
    case kind_name
    when 'notice', 'success'
      KIND_INFO[:success]
    when 'alert', 'error'
      KIND_INFO[:error]
    when 'info'
      KIND_INFO[:info]
    when 'warning'
      KIND_INFO[:warning]
    end
  end
end
