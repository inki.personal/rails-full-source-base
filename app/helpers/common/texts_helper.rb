# frozen_string_literal: true

module Common::TextsHelper
  def common_text(text_type, action_or_attribute, model: '', name: '')
    I18n.t(
      "#{text_type}.common.#{action_or_attribute}",
      name: name,
      model: I18n.t("activerecord.models.#{model}", default: '')
    )
  end

  def rescue_unknown_text(content)
    content.presence || t('labels.no_information')
  end
end
