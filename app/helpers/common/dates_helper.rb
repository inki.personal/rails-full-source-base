# frozen_string_literal: true

module Common::DatesHelper
  def date_to_time_unix(date)
    date.to_time.to_i
  rescue StandardError
    nil
  end
end
