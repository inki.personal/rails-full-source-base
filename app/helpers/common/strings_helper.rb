# frozen_string_literal: true

module Common::StringsHelper
  def to_latin(text)
    text.unicode_normalize(:nfd)
        .gsub(/[\u0300-\u1e949]/, '')
        .gsub('đ', 'd')
  end
end
