# frozen_string_literal: true

module Common::HumansHelper
  def full_name(names, locale = I18n.locale)
    return if names.blank?

    I18n.t 'human.full_name', first_name: names[:first], last_name: names[:last], locale: locale
  end
end
