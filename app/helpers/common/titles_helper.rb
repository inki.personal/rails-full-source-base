# frozen_string_literal: true

module Common::TitlesHelper
  def title(content)
    content_for :title, content
  end
end
