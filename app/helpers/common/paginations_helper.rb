# frozen_string_literal: true

module Common::PaginationsHelper
  def page_entries_info(pagy, model)
    model_name = model.model_name.human.downcase

    sanitize(
      if pagy.to == 0
        I18n.t('paginations.info.one_page.zero', name: model_name)
      elsif pagy.pages == 1
        I18n.t('paginations.info.one_page.other', name: model_name, count: pagy.count)
      else
        I18n.t('paginations.info.more_pages', name: model_name, first: pagy.from, last: pagy.to, total: pagy.count)
      end
    )
  end

  def pagy_url_for(page, pagy)
    url_for(page.vars[:params].merge(:only_path => true, page.vars[:page_param] => pagy))
  end

  def page_pagination(pagy, template: 'default', klass: {}, **parameters)
    return if pagy.to == 0 || pagy.pages == 1

    # Set remote if it is remote request
    pagy.vars[:link_extra] = 'data-remote="true"' if parameters.delete(:remote).present?
    # Reject all useless parameters in request query params
    pagy.vars[:params] = parameters

    render partial: "shared/paginations/#{template}", locals: { pagy: pagy, link: pagy_link_proc(pagy), klass: klass }
  end
end
