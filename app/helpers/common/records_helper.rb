# frozen_string_literal: true

module Common::RecordsHelper
  def error_messages_of(record, attribute)
    tag.div class: 'js-recordError-messagesBlock' do
      full_error_message_for(record, attribute).map do |message|
        tag.div message
      end.join.html_safe
    end
  end

  private

  def full_error_message_for(record, attribute)
    return record.errors.full_messages_for(attribute) if record.try(:full_error_messages).blank?

    record.full_error_messages[attribute.try(:to_sym)]
  end
end
