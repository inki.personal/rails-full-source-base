# frozen_string_literal: true

module Common::LanguagesHelper
  def current_language?(name)
    I18n.locale.to_s == name.to_s
  end

  def multiple_language_available?
    Settings.language.available.size > 1
  end

  def language_text(language_value, locale: I18n.locale)
    return if language_value.blank?

    I18n.t("shared.languages.#{language_value}", locale: locale)
  end
end
