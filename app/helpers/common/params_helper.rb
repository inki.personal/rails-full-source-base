# frozen_string_literal: true

module Common::ParamsHelper
  ALLOW_LIST = %i[page].freeze
  DENY_LIST = %i[button controller action commit].freeze

  def ransack_query
    ransack_params.to_h.to_query
  end

  def ransack_params
    @ransack_params ||= params.except(*DENY_LIST).permit(*ALLOW_LIST, q: {}).tap do |parameters|
      parameters[:q].reject! { |_, value| value.blank? } if parameters[:q].present?
    end
  end
end
