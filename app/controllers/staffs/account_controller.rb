# frozen_string_literal: true

module Staffs
  class AccountController < BaseController
    def show; end

    def update
      current_staff.update! updating_params
      notify :success, I18n.t('notifications.account.updated_successfully'), now: true
    end

    def update_password
      service = ::Account::ChangingPasswordService.new(updating_password_params)
      service.of_record(current_staff).change!
      notify :success, t('notifications.account.updated_password_successfully'), now: true
      @updated = true
    ensure
      current_staff.errors.merge! service.errors
    end

    def update_avatar
      service = Account::UpdatingAvatarService.new(current_staff, updating_avatar_params)
      service.execute!
      notify :success, t('notifications.account.updated_avatar_successfully'), now: true
      @updated = true
    ensure
      @staff = service.record
    end

    private

    def updating_params
      params.require(:staff)
            .permit(:first_name, :last_name, :birthdate, :phone_number, :address)
    end

    def updating_password_params
      params.permit(:old_password, :password, :password_confirmation)
    end

    def updating_avatar_params
      params.permit(:avatar, :offset_x, :offset_y, :width, :height)
    end
  end
end
