# frozen_string_literal: true

module Staffs
  class SessionsController < BaseController
    skip_before_action :authenticate_staff!, only: %i[new create]

    layout 'authentication'

    def new
      return redirect_to(staffs_root_path) if staff_signed_in?
    end

    def create
      (service = ::Staffs::Auth::SigningInService.new signing_in_params).verify!
      notify :success, I18n.t('notifications.auth.you_have_signed_in_successfully')
      warden.set_user service.staff, scope: :staff
    end

    def destroy
      warden.logout(:staff)
      notify :success, I18n.t('notifications.auth.you_have_signed_out_successfully')
      redirect_to staffs_sign_in_path
    end

    private

    def signing_in_params
      params.permit :email, :password
    end
  end
end
