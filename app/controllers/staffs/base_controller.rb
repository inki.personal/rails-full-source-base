# frozen_string_literal: true

class Staffs::BaseController < ApplicationController
  include Auth::StaffsHelper
  include Common::NotificationsHelper::Backend

  before_action :authenticate_staff!, except: %i[new]

  layout 'staffs'
end
