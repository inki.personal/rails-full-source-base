class ApplicationController < ActionController::Base
  MAXIMUM_RETRY_TIMES = 3

  rescue_from Exception do |e|
    error_obj = ActionError.new(object: e)
    if request.format.js?
      flash.now[:error] = error_obj.message
      render error_obj.permission_error? ? 'shared/permissions/notify_errors' : action_name
    else
      redirect_to error_redirect_path, flash: { error: error_obj.message }
    end
  end

  private

  def error_redirect_path
    retry_times = params[:retry_times].to_i + 1
    return referer_path_with_retry_times(retry_times) if request.referer.present? && retry_times <= MAXIMUM_RETRY_TIMES

    send("#{controller_path.split('/').first}_root_path")
  end

  def referer_path_with_retry_times(retry_times_count)
    uri = URI.parse(request.referer)
    new_query_params = URI.decode_www_form(uri.query || '').to_h
    new_query_params['retry_times'] = retry_times_count
    uri.query = URI.encode_www_form Array(new_query_params.to_a)
    uri.to_s
  end
end
