# frozen_string_literal: true

namespace :db do
  namespace :seed do
    desc 'Creates core data'
    task core: :environment do
      puts '== BEGIN: Create core data =='
      file_paths = Dir[Rails.root.join('db', 'seeds', 'core', '*.rb')]
      file_paths.sort_by! { |file_path| File.basename(file_path).split('_').first.to_i }
      file_paths.each { |filename| load(filename) if File.exist?(filename) }
      puts '== END: Create core data =='
    end
  end
end
