#############################
# CORE BASE
#############################
source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '3.0.2'
gem 'rails', '~> 6.1.4'
gem 'mysql2', '~> 0.5'
gem 'puma', '~> 5.0'
gem 'webpacker', '~> 5.0'
gem 'bootsnap', '>= 1.4.4', require: false
# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]

gem 'config'
gem 'sidekiq'
gem 'hiredis'
gem 'redis', '>= 4.2.5', :require => ['redis', 'redis/connection/hiredis']

#############################
# AUTHENTICATION / SECURITY
#############################
gem 'warden'
gem 'bcrypt', '~> 3.1.7'

#############################
# TEMPLATE/VIEW ENGINES
#############################
gem 'turbolinks', '~> 5'
gem 'sass-rails', '>= 6'
gem 'slim-rails', '~> 3.2'

#############################
# ENGINES
#############################
gem 'ransack', github: 'activerecord-hackery/ransack'
gem 'pagy'
gem 'image_processing'
gem 'mini_magick'

#############################
# DEVELOPMENT
#############################
group :development, :test do
  gem 'faker'
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  gem 'dotenv-rails'
  gem 'pry-byebug'
end

group :development do
  gem 'web-console', '>= 4.1.0'
  gem 'rack-mini-profiler', '~> 2.0'
  gem 'listen', '~> 3.3'
  gem 'spring'
  gem 'letter_opener'
  gem 'bullet'

  gem 'rubocop', require: false
  gem 'scss_lint', require: false
  gem 'slim_lint', require: false
end

group :test do
  gem 'capybara', '>= 3.26'
  gem 'selenium-webdriver'
  gem 'webdrivers'
end
