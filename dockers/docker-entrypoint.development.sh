#!/bin/bash
# Exit on fail
set -e

# Bundle install
bundle install --jobs `expr $(cat /proc/cpuinfo | grep -c "cpu cores") - 1` --retry 2
yarn install  --check-files
# bundle install

# Migrate
bundle exec rails db:create db:migrate

# Remove puma pid if existed
if [ -f tmp/pids/server.pid ]; then
  rm tmp/pids/server.pid
fi

# Run rails sereverss
bundle exec rails s --port=$RAILS_PORT -b 0.0.0.0 --environment=development

# Finally call command issued to the docker service
exec "$@"
