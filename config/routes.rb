Rails.application.routes.draw do
  ####### BEGIN: Staff ########
  namespace :staffs do
    get 'sign_in', to: 'sessions#new'
    resource  :sessions, only: %i[create destroy]
    resources :dashboard, only: %i[index]
    resource  :account, only: %i[show update], controller: :account do
      patch 'update_password'
      patch 'update_avatar'
    end

    root 'dashboard#index'
  end
end
