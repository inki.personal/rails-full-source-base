require_relative 'boot'

require 'rails'
# Pick the frameworks you want:
require 'active_model/railtie'
require 'active_job/railtie'
require 'active_record/railtie'
require 'active_storage/engine'
require 'action_controller/railtie'
require 'action_mailer/railtie'
require 'action_view/railtie'
# require 'action_mailbox/engine'
# require 'action_text/engine'
# require 'action_cable/engine'
# require 'sprockets/railtie'
# require 'rails/test_unit/railtie'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module VuongKhaiHaSourceBase
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 6.1

    # Configuration for the application, engines, and railties goes here.
    #
    # These settings can be overridden in specific environments using the files
    # in config/environments, which are processed later.
    #
    # config.time_zone = "Central Time (US & Canada)"
    # config.eager_load_paths << Rails.root.join("extras")

    # Disable load all helpers
    # The controller will include a helper which matches the name of the controller.
    # Eg: MyController will automatically include MyHelper
    config.action_controller.include_all_helpers = false

    # Require all locale files
    config.i18n.load_path += Dir[Rails.root.join('config', 'locales', '**', '*.{rb,yml}').to_s]
    config.i18n.default_locale = ENV.fetch('DEFAULT_LOCALE', 'vi')

    # Set timezone
    config.time_zone = ENV.fetch('TZ', 'Asia/Ho_Chi_Minh')
    config.active_record.default_timezone = :local

    # Add warden authentication middleware
    config.middleware.insert_after ActionDispatch::Flash, Warden::Manager do |manager|
    #   manager.default_strategies :authentication_strategy
    end

    # Remove fields_with_errors wrapper in action view
    config.action_view.field_error_proc = Proc.new { |html_tag, instance| html_tag.html_safe }
  end
end
