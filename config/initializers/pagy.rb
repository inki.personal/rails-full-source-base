# Items per page
Pagy::VARS[:items] = 9

# Lis page in paginaion block
# Example: [1, 1, 1, 1]
# Result: [1]...[4][5][6]...[9]
Pagy::VARS[:size] = [1, 1, 1, 1]
