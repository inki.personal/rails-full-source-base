Ransack.configure do |config|
  config.custom_arrows = {
    default_arrow: '<i class="fas fa-sort"></i>',
    up_arrow: '<i class="fas fa-sort-up"></i>',
    down_arrow: '<i class="fas fa-sort-down"></i>'
  }

  # Greater or equal to [beginning of day]
  config.add_predicate(
    'gteq_bod',
    arel_predicate: 'gteq',
    formatter: proc { |v| v.to_date.beginning_of_day },
    validator: proc { |v| v.present? }
  )

  # Greater or equal to [end of day]
  config.add_predicate(
    'lteq_eod',
    arel_predicate: 'lteq',
    formatter: proc { |v| v.to_date.end_of_day },
    validator: proc { |v| v.present? }
  )

  # Date equals
  config.add_predicate(
    'date_eq',
    arel_predicate: 'eq',
    formatter: proc { |v| v.to_date },
    validator: proc { |v| v.present? }
  )
end
