Warden::Manager.serialize_into_session(:staff) { |staff| staff.id }
Warden::Manager.serialize_from_session(:staff) { |id| Staff.find_by id: id }
