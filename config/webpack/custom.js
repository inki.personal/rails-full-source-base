const path = require('path')

module.exports = {
  resolve: {
    alias: {
      '@': path.resolve(__dirname, '..', '..', 'app/javascript'),
      '@apis': path.resolve(__dirname, '..', '..', 'app/javascript/apis'),
      '@configs': path.resolve(__dirname, '..', '..', 'app/javascript/configs'),
      '@components': path.resolve(__dirname, '..', '..', 'app/javascript/components'),
      '@helpers': path.resolve(__dirname, '..', '..', 'app/javascript/helpers'),
      '@hooks': path.resolve(__dirname, '..', '..', 'app/javascript/hooks'),
      '@images': path.resolve(__dirname, '..', '..', 'app/javascript/assets/images'),
      '@plugins': path.resolve(__dirname, '..', '..', 'app/javascript/plugins'),
      '@pages': path.resolve(__dirname, '..', '..', 'app/javascript/pages'),
      '@layouts': path.resolve(__dirname, '..', '..', 'app/javascript/layouts'),
      '@locales': path.resolve(__dirname, '..', '..', 'config/locales'),
      '@stylesheets': path.resolve(__dirname, '..', '..', 'app/javascript/assets/stylesheets'),
      '@vendors': path.resolve(__dirname, '..', '..', 'app/javascript/vendors')
    }
  }
}
